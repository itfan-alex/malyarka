(function($){
    $(document).ready(function (){
        var modal = $('#modal-window');

        $('.open-modal-window').click(function(event) {
            event.preventDefault();
            event.stopPropagation();
            modal.css({display: 'block'});
        });

        $('#modal-window-close').click(function(event) {
            event.preventDefault();
            event.stopPropagation();
            modal.css({display: 'none'});
        });
    });
})(jQuery);