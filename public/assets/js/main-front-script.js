(function($){

    /****** EVENTS ******/

    $(document).ready(function (){
        StartStageOwl();
        StartAdminPurchasesEditFileForm();
        StartAdminTopPanelConsoleToggle();
    });

    $(window).on('load', function(){
        CalcGreenLinePosition();
        StartAdminMenuCore();
    });

    $(window).on('resize', function(){
        CalcGreenLinePosition();
        StartAdminMenuCore();
        setTimeout(function (){
            CalcGreenLinePosition();
        }, 500);
    });

    /****** FUNCTIONS ******/

    function CalcGreenLinePosition(){
        var lineContainer = $('.layer-8')[0];
        if(lineContainer){
            var current_stage_button = $('.layer-6 .row.passed.current')[0]
            if(current_stage_button){
                var content_block = $('.layer-7')[0];
                var text_link = $(current_stage_button).find('a.link-text')[0];
                var line_top = $(lineContainer).find('.line-top')[0];
                var line_vertical = $(lineContainer).find('.line-vertical')[0];
                var line_bottom = $(lineContainer).find('.line-bottom')[0];
                if(content_block && text_link && line_top && line_vertical && line_bottom){
                    var text_link_width = parseInt($(text_link).width());
                    var text_link_height = parseInt($(text_link).outerHeight());
                    var text_link_offset_top = parseInt($(text_link).offset().top);
                    var text_link_offset_left = parseInt($(text_link).offset().left);
                    var content_block_width = parseInt($(content_block).width());
                    var content_block_offset_top = parseInt($(content_block).offset().top);
                    var content_block_offset_left = parseInt($(content_block).offset().left);
                    var line_top_diff = 40; /* Высота верхней ступеньки линии в px */

                    /* Calculate top line */
                    $(line_top).css({
                        "left" : (text_link_offset_left + text_link_width + 1) + "px",
                        "top" :  (content_block_offset_top + line_top_diff) + "px",
                        "width" : (content_block_offset_left - (text_link_offset_left + text_link_width) + (content_block_width/2)) + "px"
                    });

                    /* Calculate vertical line */
                    $(line_vertical).css({
                        "left" : (text_link_offset_left + text_link_width) + "px",
                        "top" :  (content_block_offset_top + line_top_diff) + "px",
                        "height" : (text_link_offset_top + text_link_height - content_block_offset_top - line_top_diff + 1) + "px"
                    });

                    /* Calculate bottom line */
                    $(line_bottom).css({
                        "width" : text_link_width+ "px",
                        "top" : (text_link_height + text_link_offset_top) + "px",
                        "left" : text_link_offset_left + "px"
                    });

                }
            }
        }
    }

    function StartStageOwl() {
        $('.layer-7 .owl-carousel').owlCarousel({
            loop : true,
            margin : 7,
            nav : true,
            dots : false,
            center : true,
            items : 1,
            autoHeight : true,
            autoplay : false,
            autoplayTimeout : 5000,
            autoplayHoverPause : true,
            navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });

        /* Auto height fix */
        var interval = setInterval(function () {
            var current_item = $('.owl-item.active').get()[0];
            var parents_outers = $(current_item).parents('.owl-stage-outer.owl-height');
            var outer = parents_outers[0];
            $(outer).height(parseInt($(current_item).height()));
        }, 500);
        setTimeout(function () {
            clearInterval(interval);
        }, 5000);
    }
    
    function StartAdminPurchasesEditFileForm() {

        /* Delete this attachment form actions*/
        $('.attachment-wrap .delete-button').each(function (){
            var form = $(this).find('form')[0];
            var del_icon = $(this).find('i')[0];
            $(del_icon).on('click', function() {
                $(form).submit();
            });
        });

        /* Add new attachment form actions */
        $('.attachment-add-block').each(function(){
            var form_block = this;
            var add_icon = $(form_block).find('i')[0];
            var form_file_input = $(form_block).find('input[type=file]')[0];
            var form = $(form_block).find('form')[0];
            $(add_icon).on('click', function(){
                $(form_file_input).click();
            });
            $(form_file_input).change(function(){
                $(form).submit();
            });
        });
    }
    
    function StartAdminTopPanelConsoleToggle() {
        $('#console-toggle').on('click', function(){
            var left_panel = $('.left-panel').get()[0];
            if($(left_panel).hasClass('active')){
                $(left_panel).removeClass('active');
            }else{
                $(left_panel).addClass('active');
            }
        });
    }
    
    function StartAdminMenuCore() {
        var buttons = $('.admin-page .left-panel ul.general-menu>li>a').each(function(){
            var button = this;
            $(button).on('click', function(){
                $(buttons).each(function() {
                    $(this).parent().find('ul.sub-menu').removeClass('tablet_visible');
                });
                if(window.innerWidth>600 && window.innerWidth<=800){
                    $(button).parent().find('ul.sub-menu').addClass('tablet_visible');
                    return false;
                }
            });
        });
    }

})(jQuery);