@extends('layouts.app')

@section('page-classes', 'home-page')

@section('content')
    <aside class="layer-1"></aside>
    <aside class="layer-2">
        <div class="figure-left"></div>
        <div class="figure-right"></div>
        <div class="figure-mob"></div>
    </aside>
    <aside class="layer-3"></aside>
    <aside class="layer-4">
        <a href="/">
            <img src="/img/logo.png" alt="{{ trans('content.site-name') }}">
        </a>
    </aside>
    <section class="layer-5">
        @include('partials._contact-information')
    </section>
    <nav class="layer-6">
        @include('partials._stages-list')
    </nav>
    @include('partials._customer-sign-in-form')
    <section class="layer-10">
        <div class="inner">
            @include('partials._customer-sign-out-form')
        </div>
    </section>
@endsection