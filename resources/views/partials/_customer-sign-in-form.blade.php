@unless($customerData)
    <section class="layer-9">
        <h2>{{ trans('forms.log-in-title') }}</h2>

        @if($errors->count())
            <p class="error-message">{{ trans('forms.purchase-not-found') }}</p>
        @endif

        {!! Form::open(['route' => 'customer.sign-in']) !!}
        <div class="block-1">
            {{ Form::text('number', null, ['required']) }}
            {{ Form::label('number', trans('forms.number')) }}
            {{ Form::text('phone', null, ['required']) }}
            {{ Form::label('phone', trans('forms.phone')) }}
        </div>
        <div class="block-2">
            {{ Form::submit(trans('forms.log-in')) }}
        </div>
        {!! Form::close() !!}
    </section>
@endunless


