@extends('layouts.app')

@section('page-classes', 'stage-page')

@section('content')
    <aside class="layer-1"></aside>
    <aside class="layer-2">
        <div class="figure-left"></div>
        <div class="figure-right"></div>
        <div class="figure-mob"></div>
        <div class="figure-tablet"></div>
    </aside>
    <aside class="layer-3"></aside>
    <aside class="layer-4">
        <a href="/">
            <img src="/img/logo.png" alt="{{ trans('content.site-name') }}">
        </a>
    </aside>
    <section class="layer-5">
        @include('partials._contact-information')
    </section>
    <nav class="layer-6">
        @include('partials._stages-list')
    </nav>
    <section class="layer-7">
        <div class="inner">
            @if(is_null($stage) || $stage->attachments->count() === 0)
                <img src="{{ url("img/stage-{$page['number']}.jpg") }}" alt="{{ trans('content.site-name') }}">
            @else
                @if($stage->attachments->count() === 1)
                    @if($stage->attachments->first()->type === 'photo')
                        <img src="{{ $stage->attachments->first()->link }}" alt="{{ trans('content.site-name') }}">
                    @else
                        <video controls>
                            <source src="{{ $stage->attachments->first()->link }}">
                        </video>
                    @endif
                @elseif($stage->attachments->count() > 1)
                    <div class="owl-carousel owl-theme">
                        @foreach($stage->attachments as $attachment)
                            @if($attachment->type === 'photo')
                                <img src="{{ $attachment->link }}" alt="{{ trans('content.site-name') }}">
                            @else
                                <video controls>
                                    <source src="{{ $attachment->link }}">
                                </video>
                            @endif
                        @endforeach
                    </div>
                @endif
            @endif
        </div>
    </section>
    <aside class="layer-8">
        <div class="line-bottom"></div>
        <div class="line-vertical"></div>
        <div class="line-top"></div>
    </aside>
    <section class="layer-10">
        <div class="inner">
            @include('partials._customer-sign-out-form')
        </div>
    </section>
@endsection
